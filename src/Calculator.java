import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.TimeZone;

public class Calculator{
	private static final String FILE_EXTENSION = ".craftcalc";
	private static final FileFilter FILE_FILTER = new FileFilter(){
		public boolean accept(File f){
			return f.isDirectory() || f.getName().endsWith(FILE_EXTENSION);
		}

		public String getDescription(){
			return L.FILE_DESCRIPTION;
		}
	};

	private static final Insets INSETS_1T = new Insets(1, 0, 0, 0);
	private static final Insets INSETS_1T_1R = new Insets(1, 0, 0, 1);
	private static final Insets INSETS_1T_2R = new Insets(1, 0, 0, 2);
	private static final Insets INSETS_2T = new Insets(2, 0, 0, 0);
	private static final Insets INSETS_2R = new Insets(0, 0, 0, 2);
	private static final Insets INSETS_2TR = new Insets(2, 0, 0, 2);
	private static final Insets INSETS_2T_1R = new Insets(2, 0, 0, 1);

	private static final byte SAVE_VERSION = 0;
	private static final byte SAVE_VERSION_MIN = 0;

	private static final String TEXT_GAP = " ";

	public static long currentTime = 0;

	private static JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir"));

	public static String gameName;

	public static Item[] items;

	public static Timer itemTimer;

	public static int[] productionGroupSlots;

	public static JButton[] requestButtons = new JButton[0];
	public static int requestColumns;

	private static File saveFile;

	public static JFrame frame;
	private static int frameMaxHeight = 0;
	private static int frameWidth = 0;
	private static JPanel viewPanel = new JPanel(new GridBagLayout());

	public static void main(String[] args){
		Util.DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));

		new Calculator();
	}

	public Calculator(){
		UIManager.put("Button.foreground", Color.BLACK);

		UIManager.put("Label.background", Color.WHITE);
		UIManager.put("Label.foreground", Color.BLACK);

		UIManager.put("ScrollPane.border", new EmptyBorder(0, 0, 0, 0));

		UIManager.put("TextField.foreground", Color.BLACK);

		fileChooser.resetChoosableFileFilters();
		fileChooser.setAcceptAllFileFilterUsed(false);
		fileChooser.setFileFilter(FILE_FILTER);
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

		JFrame temp = new JFrame(L.TITLE);
		temp.setUndecorated(true);
		temp.setVisible(true);

		int choice = 0;
		while(choice >= 0){
			choice = JOptionPane.showOptionDialog(null, L.LAUNCH_PROMPT, L.TITLE, JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, L.LAUNCH_OPTIONS, null);
			if(choice == 0){
				if(fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
					saveFile = fileChooser.getSelectedFile();
					if(load()){
						sortItems(false);
						break;
					}
				}
			}
			else if(choice == 1){
				gameName = Util.displayInputPrompt(L.OPTIONS_GAME_NAME, Calculator.gameName, null, L.OPTIONS_GAME_NAME_PROMPT, Util::validString, L.ERROR_GAME_NAME_INVALID);
				if(gameName != null && gameName.length() > 0){
					items = new Item[0];
					productionGroupSlots = new int[1];
					requestColumns = 1;
					break;
				}
			}
		}
		temp.dispose();
		if(choice >= 0){
			init();
		}
	}

	/**
	 Checks if an item is used in a recipe.
	 @param item Item to check for.
	 @return Object[]{<br>boolean True if the item is in a recipe,<br>String Message describing items it is part of, null if its not in a recipe.<br>}
	 */
	public static Object[] itemInRecipe(Item item){
		boolean pass = true;
		StringBuilder message = new StringBuilder(Util.EMPTY);
		for(Item checkItem : items){
			if(checkItem.hasComponent(item)){
				pass = false;
				message.append(Util.NL).append(checkItem.name);
			}
		}
		return new Object[]{pass, pass ? null : message.toString()};
	}


	/**
	 Adds an item to the items list.
	 @param name The name of the new item.
	 */
	public static void addItem(String name){
		items = Util.resizeArray(items, 1);
		items[items.length - 1] = new Item(name, 1, 0);
	}

	/**
	 Removes an item from the items list.
	 @param item The item.
	 */
	public static void removeItem(Item item){
		for(int i = 0; i < items.length; i++){
			if(items[i].equals(item)){
				items[i] = null;
				break;
			}
		}
		items = Util.resizeArray(items, -1);
	}

	public static void calculate(){
		for(Item item : items){
			item.resetItem();
		}
		for(JButton button : requestButtons){
			button.setBackground(Util.COLOR_BUTTON);
		}
		HashMap<Item, Integer> neededAmounts = new HashMap<>();
		for(Item item : items){
			neededAmounts.put(item, -item.inventoryAmount - (item.collectTimes.size() * item.amountPerCraft));
		}
		Item component, neededItem;
		int amount;
		boolean canCraft;
		for(int i = items.length - 1; i >= 0; i--){
			neededItem = items[i];
			amount = neededAmounts.get(neededItem) + neededItem.getRequestAmount();
			if(amount > 0){
				canCraft = (boolean) neededItem.canCraft(1)[0];
				if(canCraft){
					neededItem.inventoryButton.setBackground(Color.GREEN);
				}
				for(int j = 0; j < neededItem.recipeComponents.length; j++){
					component = neededItem.recipeComponents[j];
					if(canCraft){
						component.processLabel.setBackground(Color.GREEN);
					}
					neededAmounts.put(component, neededAmounts.get(component) + (neededItem.recipeAmounts[j] * amount));
				}
			}
			neededAmounts.put(neededItem, amount);
		}
		for(Item item : items){
			item.neededLabel.setText(Util.EMPTY+Math.max(neededAmounts.get(item), 0));
			item.processLabel.setText(Util.EMPTY+(int) Math.max(Math.ceil((double) neededAmounts.get(item) / (double) item.amountPerCraft), 0));
		}
		if(items.length > 0){
			boolean hasRequest, pass;
			for(int i = 0; i < requestColumns; i++){
				hasRequest = false;
				pass = true;
				for(Item item : items){
					try{
						if(Integer.parseInt(item.requestFields[i].getText()) > item.inventoryAmount){
							pass = false;
						}
						else{
							item.requestFields[i].setBackground(Color.GREEN);
						}
						hasRequest = true;
					}catch(NumberFormatException ignored){}
				}
				if(hasRequest && pass){
					requestButtons[i].setBackground(Color.GREEN);
				}
			}
		}
	}

	private static void init(){
		frame = new JFrame();
		frame.setContentPane(new JScrollPane(viewPanel, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER));
		frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		frame.setLocationByPlatform(true);
		frame.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				int choice = JOptionPane.showConfirmDialog(frame, L.SAVE_PROMPT, L.TITLE + L.SEP + L.SAVE_TITLE, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if(choice >= 0){
					if(choice == JOptionPane.NO_OPTION || save()){
						frame.dispose();
					}
				}
			}
		});
		frame.addComponentListener(new ComponentAdapter(){
			public void componentResized(ComponentEvent e){
				if(frameMaxHeight > 0){
					if(frame.getHeight() > frameMaxHeight){
						frame.setEnabled(false);
						frame.setSize(frameWidth, frameMaxHeight);
						frame.setEnabled(true);
					}
					if(frame.getWidth() != frameWidth){
						frame.setEnabled(false);
						frame.setSize(frameWidth, frame.getHeight());
						frame.setEnabled(true);
					}
				}
			}
		});
		viewPanel.setBackground(Color.BLACK);

		itemTimer = new Timer(2000, e->{
			updateCollectButtons();
		});
		itemTimer.stop();

		rebuildComponents();
	}

	public static void rebuildComponents(){
		frame.setTitle(L.TITLE+L.SEP+gameName);
		viewPanel.removeAll();
		GridBagConstraints c = new GridBagConstraints();

		JButton button = new JButton(L.MAIN_HEADERS[0]);
		button.setBackground(Util.COLOR_BUTTON);
		button.setMargin(Util.INSETS_0);
		button.addActionListener(e->{
			itemTimer.stop();
			frame.setEnabled(false);
			new OptionsMenu();
		});
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = INSETS_2R;
		c.ipadx = 4;
		c.ipady = 2;
		c.weightx = 1;
		viewPanel.add(button, c);

		JLabel label = new JLabel(L.MAIN_HEADERS[1], SwingConstants.CENTER);
		label.setOpaque(true);
		c.gridx++;
		c.gridwidth = requestColumns;
		viewPanel.add(label, c);

		label = new JLabel(L.MAIN_HEADERS[2], SwingConstants.CENTER);
		label.setOpaque(true);
		c.gridwidth = 1;
		c.gridx += requestColumns;
		viewPanel.add(label, c);

		label = new JLabel(L.MAIN_HEADERS[3], SwingConstants.CENTER);
		label.setOpaque(true);
		c.gridx++;
		viewPanel.add(label, c);

		int maxGroup = 0;
		for(Item item : items){
			item.createRequestFields(null);
			maxGroup = Math.max(maxGroup, item.group);
		}
		label = new JLabel(L.MAIN_HEADERS[4], SwingConstants.CENTER);
		label.setOpaque(true);
		c.gridwidth = maxGroup + 1;
		c.gridx++;
		viewPanel.add(label, c);

		label = new JLabel(L.MAIN_HEADERS[5], SwingConstants.CENTER);
		label.setOpaque(true);
		c.gridwidth = 1;
		c.gridx += maxGroup + 1;
		viewPanel.add(label, c);

		label = new JLabel(L.MAIN_HEADERS[6], SwingConstants.CENTER);
		label.setOpaque(true);
		c.gridx++;
		c.insets = Util.INSETS_0;
		viewPanel.add(label, c);

		int curGroup = -1;
		for(Item item : items){
			label = new JLabel(item.name+TEXT_GAP, SwingConstants.RIGHT);
			label.setOpaque(true);
			c.gridx = 0;
			c.gridy++;
			c.insets = item.group == curGroup ? INSETS_1T_2R : INSETS_2TR;
			c.ipadx = 4;
			viewPanel.add(label, c);

			c.insets = item.group == curGroup ? INSETS_1T_1R : INSETS_2T_1R;
			c.ipadx = 0;
			for(int i = 0; i < requestColumns; i++){
				c.gridx++;
				if(i == requestColumns - 1){
					c.insets = item.group == curGroup ? INSETS_1T_2R : INSETS_2TR;
				}
				viewPanel.add(item.requestFields[i], c);
			}

			c.gridx++;
			viewPanel.add(item.craftField, c);

			c.gridx++;
			viewPanel.add(item.inventoryButton, c);

			c.insets = item.group == curGroup ? INSETS_1T_1R : INSETS_2T_1R;
			for(int i = 0; i < maxGroup + 1; i++){
				c.gridx++;
				if(i == maxGroup){
					c.insets = item.group == curGroup ? INSETS_1T_2R : INSETS_2TR;
				}
				viewPanel.add(i == item.group ? item.processLabel : new JLabel(), c);
			}

			c.gridx++;
			c.insets = item.group == curGroup ? INSETS_1T_2R : INSETS_2TR;
			viewPanel.add(item.neededLabel, c);

			c.gridx++;
			c.insets = item.group == curGroup ? INSETS_1T : INSETS_2T;
			viewPanel.add(item.collectButton, c);

			curGroup = item.group;
		}
		if(items.length > 0){
			requestButtons = new JButton[requestColumns];
			c.gridx = 0;
			c.gridy++;
			c.insets = INSETS_2T_1R;
			for(int i = 0; i < requestColumns; i++){
				button = new JButton(L.MAIN_X);
				button.setBackground(Util.COLOR_BUTTON);
				button.setMargin(Util.INSETS_0);
				button.setPreferredSize(Util.DIMENSION_COMPONENTS);
				int index = i;
				button.addActionListener(e->{
					boolean hasRequest = false;
					boolean pass = true;
					for(Item item : items){
						try{
							if(Integer.parseInt(item.requestFields[index].getText()) > item.inventoryAmount){
								pass = false;
								break;
							}
							hasRequest = true;
						}catch(NumberFormatException ignored){}
					}
					if(hasRequest && pass && JOptionPane.showConfirmDialog(frame, L.MAIN_FILL_REQUEST.replace(Util.REP, Util.EMPTY+(index + 1)), L.TITLE, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
						for(Item item : items){
							try{
								item.changeInventoryAmount(-Integer.parseInt(item.requestFields[index].getText()));
								item.requestFields[index].setText(null);
							}catch(NumberFormatException ignored){}
						}
						calculate();
					}
				});
				requestButtons[i] = button;
				c.gridx++;
				if(i == requestColumns - 1){
					c.insets = INSETS_2TR;
				}
				viewPanel.add(button, c);
			}
		}

		frameMaxHeight = 0;
		frame.pack();
		Dimension size = frame.getSize();
		frameWidth = size.width;
		frameMaxHeight = Math.min(size.height, GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().height);
		frame.setSize(frameWidth, frameMaxHeight);

		calculate();
		frame.setVisible(true);
		frame.setEnabled(true);
		itemTimer.start();
		updateCollectButtons();
	}

	/**
	 Sorts the item array by group and name and each item's recipe array.
	 @param doRecipes True if item recipes should be sorted.
	 */
	public static void sortItems(boolean doRecipes){
		for(Item item : items){
			item.group = -1;
		}
		for(Item item : items){
			item.setupGroup();
		}
		Arrays.sort(items);
		if(doRecipes){
			HashMap<Integer, Item> oldIndexToItem;
			HashMap<Item, Integer> itemToNewIndex;
			for(Item item : items){
				oldIndexToItem = new HashMap<>();
				for(int i = 0; i < item.recipeComponents.length; i++){
					oldIndexToItem.put(i, item.recipeComponents[i]);
				}
				Arrays.sort(item.recipeComponents);
				itemToNewIndex = new HashMap<>();
				for(int i = 0; i < item.recipeComponents.length; i++){
					itemToNewIndex.put(item.recipeComponents[i], i);
				}
				Integer[] newRecipeAmounts = new Integer[item.recipeComponents.length];
				for(int i = 0; i < item.recipeComponents.length; i++){
					newRecipeAmounts[itemToNewIndex.get(oldIndexToItem.get(i))] = item.recipeAmounts[i];
				}
				item.recipeAmounts = newRecipeAmounts;
			}
		}
	}

	public static void updateCollectButtons(){
		currentTime = System.currentTimeMillis();
		for(Item item : items){
			item.updateCollect();
		}
	}


	private static boolean save(){
		if(saveFile == null){
			if(fileChooser.showSaveDialog(frame) == JFileChooser.APPROVE_OPTION){
				saveFile = fileChooser.getSelectedFile();
				if(!saveFile.getPath().endsWith(FILE_EXTENSION)){
					saveFile = new File(saveFile.getPath()+FILE_EXTENSION);
				}
			}
		}
		if(saveFile != null){
			try(DataOutputStream output = new DataOutputStream(new FileOutputStream(saveFile))){
				output.writeByte(SAVE_VERSION);
				output.writeUTF(gameName);
				output.writeInt(requestColumns);
				output.writeInt(items.length);
				output.writeInt(productionGroupSlots.length);
				for(int i = 1; i < productionGroupSlots.length; i++){
					output.writeInt(productionGroupSlots[i]);
				}
				byte options;
				for(Item item : items){
					output.writeUTF(item.name);
					output.writeInt(item.amountPerCraft);
					output.writeInt(item.inventoryAmount);
					for(JTextField field : item.requestFields){
						try{
							output.writeInt(Integer.parseInt(field.getText()));
						}catch(NumberFormatException e){
							output.writeInt(0);
						}
					}
					options = 0;
					if(item.collectChain){
						options += 1;
					}
					output.writeByte(options);
					output.writeLong(item.collectDelay);
					output.writeInt(item.collectTimes.size());
					for(long time : item.collectTimes){
						output.writeLong(time);
					}
					output.writeInt(item.productionGroup);
				}
				for(Item item : items){
					output.writeInt(item.recipeComponents.length);
					for(Item component : item.recipeComponents){
						for(int i = 0; i < items.length; i++){
							if(items[i].equals(component)){
								output.writeInt(i);
								break;
							}
						}
					}
					for(int componentAmount : item.recipeAmounts){
						output.writeInt(componentAmount);
					}
				}
				return true;
			}catch(Exception e){
				e.printStackTrace();
			}
			JOptionPane.showMessageDialog(frame, L.ERROR_SAVING.replace(Util.REP, saveFile.getName()), L.TITLE+L.SEP+L.ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
		}
		return false;
	}

	private static boolean load(){
		if(saveFile.isFile() && saveFile.canRead()){
			try(DataInputStream input = new DataInputStream(new FileInputStream(saveFile))){
				byte version = input.readByte();
				if(version <= SAVE_VERSION && version >= SAVE_VERSION_MIN){
					gameName = input.readUTF();
					requestColumns = input.readInt();
					int size = input.readInt();
					items = new Item[size];
					int size2 = input.readInt();
					productionGroupSlots = new int[size2];
					for(int i = 1; i < size2; i++){
						productionGroupSlots[i] = input.readInt();
					}
					Item item;
					int[] requestAmounts = new int[requestColumns];
					byte options;
					for(int i = 0; i < size; i++){
						item = new Item(input.readUTF(), input.readInt(), input.readInt());
						for(int j = 0; j < requestColumns; j++){
							requestAmounts[j] = input.readInt();
						}
						item.createRequestFields(requestAmounts);
						options = input.readByte();
						if(options >= 1){
							item.collectChain = true;
						}
						item.collectDelay = input.readLong();
						size2 = input.readInt();
						for(int j = 0; j < size2; j++){
							item.collectTimes.add(input.readLong());
						}
						item.collectButton.setText(Util.EMPTY+size2);
						item.productionGroup = input.readInt();
						items[i] = item;
					}
					Item[] recipeComponents;
					Integer[] recipeAmounts;
					for(int i = 0; i < size; i++){
						size2 = input.readInt();
						recipeComponents = new Item[size2];
						for(int j = 0; j < size2; j++){
							recipeComponents[j] = items[input.readInt()];
						}
						items[i].recipeComponents = recipeComponents;
						recipeAmounts = new Integer[size2];
						for(int j = 0; j < size2; j++){
							recipeAmounts[j] = input.readInt();
						}
						items[i].recipeAmounts = recipeAmounts;
					}
					return true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		JOptionPane.showMessageDialog(frame, L.ERROR_LOADING.replace(Util.REP, saveFile.getName()), L.TITLE+L.SEP+L.ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
		return false;
	}
}