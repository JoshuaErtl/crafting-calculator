public class L{
	public static String ERROR_DELAY_NOT_OVER = "Items are not ready to be collected!";
	public static String ERROR_GAME_NAME_INVALID = "This is not a usable game name!";
	public static String ERROR_ITEM_IN_RECIPE = "This item is in recipes!";
	public static String ERROR_ITEM_NAME_INVALID = "This name is already used for another item!";
	public static String ERROR_LOADING = "The file\n\"%S\"\ncould not be loaded properly!";
	public static String ERROR_NO_ITEMS = "There are no items!";
	public static String ERROR_NO_PRODUCTION_SLOTS = "There are no different production groups!";
	public static String ERROR_NO_USABLE_COMPONENTS = "There are no items left to add!";
	public static String ERROR_NO_USED_COMPONENTS = "There are no components to this item!";
	public static String ERROR_SAVING = "The file\n\"%S\"\ncould not be saved properly!";
	public static String ERROR_TITLE = "Error";

	public static String FILE_DESCRIPTION = "Crafting Calculator File (.craftcalc)";

	public static String ITEM_ADD_COLLECT = "Collect %S?";
	public static String ITEM_ADD_CRAFT = "Craft %S?";
	public static String ITEM_ADD_FORCE = "Force add %S?";
	public static String ITEM_FORCE_COLLECT = "Force collect upcoming %S?";
	public static String ITEM_MISSING_COMPONENTS = "Cannot craft!  Missing:";
	public static String ITEM_MISSING_ITEM = "Cannot disassemble!  Missing:";
	public static String ITEM_PRODUCTION_FULL = "All production slots are in use!";
	public static String ITEM_REMOVE_CRAFT = "Disassemble %S?";
	public static String ITEM_REMOVE_FORCE = "Force remove %S?";
	public static String ITEM_X = " x ";

	public static String[] LAUNCH_OPTIONS = {"Load Existing", "Create New"};
	public static String LAUNCH_PROMPT = "Load existing or create new tracker?";

	public static String MAIN_FILL_REQUEST = "Fill request %S?";
	public static String[] MAIN_HEADERS = {"Item", "Request", "Craft", "Inv.", "Process", "Needed", "Collect"};
	public static String MAIN_X = "X";

	public static String OPTIONS_BASE_LABEL = "Base Options";
	public static String OPTIONS_PRODUCTION_SLOTS = "Change Production Slots";
	public static String OPTIONS_GAME_NAME = "Game Name";
	public static String OPTIONS_GAME_NAME_PROMPT = "Input game name:";
	public static String OPTIONS_ITEMS_ADD = "Add Item";
	public static String OPTIONS_ITEMS_ADD_PROMPT = "Input item name:";
	public static String OPTIONS_ITEMS_LABEL = "Items";
	public static String OPTIONS_ITEMS_MODIFY = "Modify Item";
	public static String OPTIONS_ITEMS_MODIFY_AMOUNT_PROMPT = "Choose amount of %S is made per craft:";
	public static String[] OPTIONS_ITEMS_MODIFY_BUTTONS = {"Change Name", "Add Recipe Component", "Remove Recipe Component", "Change Amount Per Craft", "Change Production"};
	public static String OPTIONS_ITEMS_MODIFY_PRODUCTION_NEW = "New production group";
	public static String OPTIONS_ITEMS_MODIFY_PRODUCTION_NONE = "No production group";
	public static String OPTIONS_ITEMS_MODIFY_PRODUCTION_PROMPT_1 = "<html>Choose amount of time each<br>%S craft takes (seconds):</html>";
	public static String OPTIONS_ITEMS_MODIFY_PRODUCTION_PROMPT_2 = "Are items crafted individually after each other?";
	public static String OPTIONS_ITEMS_MODIFY_PRODUCTION_PROMPT_3 = "Choose a production group:";
	public static String OPTIONS_ITEMS_MODIFY_PROMPT_1 = "Choose an item to modify:";
	public static String OPTIONS_ITEMS_MODIFY_PROMPT_2 = "Choose what to modify on %S:";
	public static String OPTIONS_ITEMS_MODIFY_RECIPE_ADD_PROMPT_1 = "Choose an item to add to %S recipe:";
	public static String OPTIONS_ITEMS_MODIFY_RECIPE_ADD_PROMPT_2 = "<html>Choose amount of %S<br>in recipe of %S:</html>";
	public static String OPTIONS_ITEMS_MODIFY_RECIPE_REMOVE_PROMPT = "Choose an item to remove from %S recipe:";
	public static String OPTIONS_ITEMS_REMOVE = "Remove Item";
	public static String OPTIONS_ITEMS_REMOVE_PROMPT = "Choose an item to remove:";
	public static String OPTIONS_PRODUCTION_SLOTS_PROMPT_1 = "Choose a production group:";
	public static String OPTIONS_PRODUCTION_SLOTS_PROMPT_2 = "<html>Choose amount of slots for this production group:<br>%S</html>";
	public static String OPTIONS_REQUEST_COLUMNS = "Request Columns";
	public static String OPTIONS_REQUEST_COLUMNS_PROMPT = "Choose amount of request columns:";
	public static String OPTIONS_RESET_REQUESTS = "Reset Requests";
	public static String OPTIONS_RESET_REQUESTS_DONE = "Requests reset!";
	public static String OPTIONS_TITLE = "Options";

	public static String SAVE_PROMPT = "Do you want to save before exiting?";
	public static String SAVE_TITLE = "Save";

	public static String SEP = " - ";
	public static String TITLE = "Crafting Calculator";
}