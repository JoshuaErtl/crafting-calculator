import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class OptionsMenu extends JFrame{
	public OptionsMenu(){
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setLayout(new GridLayout(0, 4, 5, 10));
		setTitle(L.TITLE+L.SEP+Calculator.gameName+L.SEP+L.OPTIONS_TITLE);
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				dispose();
				Calculator.rebuildComponents();
			}
		});

		// Row 1
		add(new JLabel(L.OPTIONS_BASE_LABEL, SwingConstants.RIGHT));

		JButton button = new JButton(L.OPTIONS_GAME_NAME);
		button.addActionListener(e->{
			String name = Util.displayInputPrompt(L.OPTIONS_GAME_NAME, Calculator.gameName, this, L.OPTIONS_GAME_NAME_PROMPT, Util::validString, L.ERROR_GAME_NAME_INVALID);
			if(name != null){
				Calculator.gameName = name;
				Calculator.frame.setTitle(L.TITLE+L.SEP+Calculator.gameName);
				setTitle(L.TITLE+L.SEP+Calculator.gameName+L.SEP+L.OPTIONS_TITLE);
			}
		});
		add(button);

		button = new JButton(L.OPTIONS_REQUEST_COLUMNS);
		button.addActionListener(e->{
			Integer amount = Util.displaySpinnerPrompt(L.OPTIONS_REQUEST_COLUMNS, L.OPTIONS_REQUEST_COLUMNS_PROMPT, 1, Calculator.requestColumns, 10, this);
			if(amount != null){
				Calculator.requestColumns = amount;
			}
		});
		add(button);

		button = new JButton(L.OPTIONS_PRODUCTION_SLOTS);
		button.addActionListener(e->{
			Integer[] items = new Integer[Calculator.productionGroupSlots.length - 1];
			String[] itemNames = new String[items.length];
			for(int i = 0; i < items.length; i++){
				items[i] = i + 1;
			}
			for(Item item : Calculator.items){
				if(item.productionGroup > 0){
					if(itemNames[item.productionGroup - 1] == null){
						itemNames[item.productionGroup - 1] = item.name;
					}
					else{
						itemNames[item.productionGroup - 1] += L.SEP+item.name;
					}
				}
			}
			Integer choice = Util.displaySelectionPrompt(L.OPTIONS_PRODUCTION_SLOTS, items, this, L.ERROR_NO_PRODUCTION_SLOTS, L.OPTIONS_PRODUCTION_SLOTS_PROMPT_1, itemNames);
			if(choice != null){
				Integer amount = Util.displaySpinnerPrompt(L.OPTIONS_PRODUCTION_SLOTS, L.OPTIONS_PRODUCTION_SLOTS_PROMPT_2.replace(Util.REP, itemNames[choice - 1]), 1, Calculator.productionGroupSlots[choice], Integer.MAX_VALUE, this);
				if(amount != null){
					Calculator.productionGroupSlots[choice] = amount;
				}
			}
		});
		add(button);

		// Row 2
		add(new JLabel(L.OPTIONS_ITEMS_LABEL, SwingConstants.RIGHT));

		button = new JButton(L.OPTIONS_ITEMS_ADD);
		button.addActionListener(e->{
			String name = Util.displayInputPrompt(L.OPTIONS_ITEMS_ADD, null, this, L.OPTIONS_ITEMS_ADD_PROMPT, OptionsMenu::itemNameExists, L.ERROR_ITEM_NAME_INVALID);
			if(name != null){
				Calculator.addItem(name);
				Calculator.sortItems(false);
			}
		});
		add(button);

		button = new JButton(L.OPTIONS_ITEMS_MODIFY);
		button.addActionListener(e->{
			String[] itemNames = new String[Calculator.items.length];
			for(int i = 0; i < itemNames.length; i++){
				itemNames[i] = Calculator.items[i].name;
			}
			Item item = Util.displaySelectionPrompt(L.OPTIONS_ITEMS_MODIFY, Calculator.items, this, L.ERROR_NO_ITEMS, L.OPTIONS_ITEMS_MODIFY_PROMPT_1, itemNames);
			if(item != null){
				int choice = 0;
				while(choice >= 0){
					choice = JOptionPane.showOptionDialog(this, L.OPTIONS_ITEMS_MODIFY_PROMPT_2.replace(Util.REP, item.name), L.TITLE+L.SEP+L.OPTIONS_ITEMS_MODIFY, JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, L.OPTIONS_ITEMS_MODIFY_BUTTONS, null);
					if(choice == 0){
						String name = Util.displayInputPrompt(L.OPTIONS_ITEMS_MODIFY+L.SEP+L.OPTIONS_ITEMS_MODIFY_BUTTONS[0], item.name, this, L.OPTIONS_ITEMS_ADD_PROMPT, OptionsMenu::itemNameExists, L.ERROR_ITEM_NAME_INVALID);
						if(name != null){
							item.name = name;
							Calculator.sortItems(true);
						}
					}
					else if(choice == 1){
						Item[] items = new Item[Calculator.items.length - item.recipeComponents.length - 1];
						itemNames = new String[items.length];
						int insertIndex = 0;
						if(items.length > 0){
							for(Item possibleComponent : Calculator.items){
								if(!possibleComponent.equals(item) && !item.hasComponent(possibleComponent)){
									items[insertIndex] = possibleComponent;
									itemNames[insertIndex] = possibleComponent.name;
									insertIndex++;
								}
							}
						}
						Item component = Util.displaySelectionPrompt(L.OPTIONS_ITEMS_MODIFY+L.SEP+L.OPTIONS_ITEMS_MODIFY_BUTTONS[1], items, this, L.ERROR_NO_USABLE_COMPONENTS, L.OPTIONS_ITEMS_MODIFY_RECIPE_ADD_PROMPT_1.replace(Util.REP, item.name), itemNames);
						if(component != null){
							Integer amount = Util.displaySpinnerPrompt(L.OPTIONS_ITEMS_MODIFY+L.SEP+L.OPTIONS_ITEMS_MODIFY_BUTTONS[1], L.OPTIONS_ITEMS_MODIFY_RECIPE_ADD_PROMPT_2.replaceFirst(Util.REP, component.name).replaceFirst(Util.REP, item.name), 1, 1, Integer.MAX_VALUE, this);
							if(amount != null){
								item.addComponent(component, amount);
								Calculator.sortItems(true);
							}
						}
					}
					else if(choice == 2){
						itemNames = new String[item.recipeComponents.length];
						for(int i = 0; i < itemNames.length; i++){
							itemNames[i] = item.recipeComponents[i].name;
						}
						Item component = Util.displaySelectionPrompt(L.OPTIONS_ITEMS_MODIFY+L.SEP+L.OPTIONS_ITEMS_MODIFY_BUTTONS[2], item.recipeComponents, this, L.ERROR_NO_USED_COMPONENTS, L.OPTIONS_ITEMS_MODIFY_RECIPE_REMOVE_PROMPT.replace(Util.REP, item.name), itemNames);
						if(component != null){
							item.removeComponent(component);
							Calculator.sortItems(true);
						}
					}
					else if(choice == 3){
						Integer amount = Util.displaySpinnerPrompt(L.OPTIONS_ITEMS_MODIFY+L.SEP+L.OPTIONS_ITEMS_MODIFY_BUTTONS[3], L.OPTIONS_ITEMS_MODIFY_AMOUNT_PROMPT.replace(Util.REP, item.name), 1, item.amountPerCraft, Integer.MAX_VALUE, this);
						if(amount != null){
							item.amountPerCraft = amount;
						}
					}
					else if(choice == 4){
						Integer amount = Util.displaySpinnerPrompt(L.OPTIONS_ITEMS_MODIFY+L.SEP+L.OPTIONS_ITEMS_MODIFY_BUTTONS[4], L.OPTIONS_ITEMS_MODIFY_PRODUCTION_PROMPT_1.replace(Util.REP, item.name), 0, (int) (item.collectDelay / 1000), Integer.MAX_VALUE, this);
						if(amount != null){
							item.collectDelay = amount * 1000;
							choice = JOptionPane.showConfirmDialog(this, L.OPTIONS_ITEMS_MODIFY_PRODUCTION_PROMPT_2, L.TITLE+L.SEP+L.OPTIONS_ITEMS_MODIFY+L.SEP+L.OPTIONS_ITEMS_MODIFY_BUTTONS[4], JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
							if(choice == JOptionPane.YES_OPTION || choice == JOptionPane.NO_OPTION){
								item.collectChain = choice == JOptionPane.YES_OPTION;
								Integer[] items = new Integer[Calculator.productionGroupSlots.length + 1];
								itemNames = new String[items.length];
								items[0] = 0;
								itemNames[0] = L.OPTIONS_ITEMS_MODIFY_PRODUCTION_NONE;
								items[items.length - 1] = items.length - 1;
								itemNames[items.length - 1] = L.OPTIONS_ITEMS_MODIFY_PRODUCTION_NEW;
								for(Item productionItem : Calculator.items){
									items[productionItem.productionGroup] = productionItem.productionGroup;
									if(productionItem.productionGroup > 0){
										if(itemNames[productionItem.productionGroup] == null){
											itemNames[productionItem.productionGroup] = productionItem.name;
										}
										else{
											itemNames[productionItem.productionGroup] += L.SEP+productionItem.name;
										}
									}
								}
								Integer productionGroup = Util.displaySelectionPrompt(L.OPTIONS_ITEMS_MODIFY+L.SEP+L.OPTIONS_ITEMS_MODIFY_BUTTONS[4], items, this, null, L.OPTIONS_ITEMS_MODIFY_PRODUCTION_PROMPT_3, itemNames);
								if(productionGroup != null){
									if(productionGroup == items.length - 1){
										int[] newProductionGroupSlots = new int[Calculator.productionGroupSlots.length + 1];
										System.arraycopy(Calculator.productionGroupSlots, 0, newProductionGroupSlots, 0, Calculator.productionGroupSlots.length);
										newProductionGroupSlots[productionGroup] = 1;
										Calculator.productionGroupSlots = newProductionGroupSlots;
									}
									else if(item.productionGroup > 0 && itemNames[item.productionGroup].equals(item.name) && item.productionGroup != productionGroup){
										int[] newProductionGroupSlots = new int[Calculator.productionGroupSlots.length - 1];
										int insertIndex = 0;
										for(int i = 0; i < Calculator.productionGroupSlots.length; i++){
											if(i != item.productionGroup){
												newProductionGroupSlots[insertIndex] = Calculator.productionGroupSlots[i];
												insertIndex++;
											}
										}
										for(Item productionItem : Calculator.items){
											if(productionItem.productionGroup > item.productionGroup){
												productionItem.productionGroup--;
											}
										}
										Calculator.productionGroupSlots = newProductionGroupSlots;
									}
									item.productionGroup = productionGroup;
									Calculator.sortItems(true);
								}
							}
						}
					}
				}
			}
		});
		add(button);

		button = new JButton(L.OPTIONS_ITEMS_REMOVE);
		button.addActionListener(e->{
			String[] itemNames = new String[Calculator.items.length];
			for(int i = 0; i < itemNames.length; i++){
				itemNames[i] = Calculator.items[i].name;
			}
			Item item = Util.displaySelectionPrompt(L.OPTIONS_ITEMS_REMOVE, Calculator.items, this, L.ERROR_NO_ITEMS, L.OPTIONS_ITEMS_REMOVE_PROMPT, itemNames);
			if(item != null){
				Object[] data = Calculator.itemInRecipe(item);
				if((boolean) data[0]){
					Calculator.removeItem(item);
					Calculator.sortItems(false);
				}
				else{
					JOptionPane.showMessageDialog(this, L.ERROR_ITEM_IN_RECIPE+data[1], L.TITLE+L.SEP+L.OPTIONS_ITEMS_REMOVE, JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		add(button);

		// Row 3
		add(new JLabel());

		button = new JButton(L.OPTIONS_RESET_REQUESTS);
		button.addActionListener(e->{
			for(Item item : Calculator.items){
				for(JTextField field : item.requestFields){
					field.setText(null);
				}
			}
			JOptionPane.showMessageDialog(this, L.OPTIONS_RESET_REQUESTS_DONE, L.TITLE+L.OPTIONS_RESET_REQUESTS, JOptionPane.INFORMATION_MESSAGE);
		});
		add(button);

		pack();
		setResizable(false);
		setLocationRelativeTo(Calculator.frame);
		setVisible(true);
	}

	/**
	 Checks if a name is already used by an item.
	 @param name The name to check for.
	 @return True if the name exists.
	 */
	private static boolean itemNameExists(String name){
		for(Item item : Calculator.items){
			if(item.name.equals(name)){
				return true;
			}
		}
		return false;
	}
}