import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class Item implements Comparable{
	private static final ActionListener ACTION_LISTENER_REQUEST = e->Calculator.calculate();

	public int amountPerCraft = 1;
	public JButton collectButton = new JButton();
	public boolean collectChain = false;
	private boolean collectCheck = true;
	public long collectDelay = 0;
	public ArrayList<Long> collectTimes = new ArrayList<>(1);
	public JTextField craftField = new JTextField();
	public int group = -1;
	public int inventoryAmount = 0;
	public JButton inventoryButton = new JButton();
	public String name;
	public JLabel neededLabel = new JLabel(Util.EMPTY, SwingConstants.CENTER);
	public JLabel processLabel = new JLabel(Util.EMPTY, SwingConstants.CENTER);
	public int productionGroup = 0;
	public Integer[] recipeAmounts = new Integer[0];
	public Item[] recipeComponents = new Item[0];
	public JTextField[] requestFields = new JTextField[0];

	public Item(String name, int amountPerCraft, int inventoryAmount){
		this.amountPerCraft = amountPerCraft;
		this.name = name;

		collectButton.setBackground(Color.BLACK);
		collectButton.setEnabled(false);
		collectButton.setMargin(Util.INSETS_0);
		collectButton.setPreferredSize(Util.DIMENSION_COMPONENTS);
		collectButton.addActionListener(e->{
			if(collectTimes.size() > 0){
				int amount = 0;
				for(long time : collectTimes){
					if(time > Calculator.currentTime){
						break;
					}
					amount++;
				}
				if(amount == 0){
					StringBuilder message = new StringBuilder(L.ERROR_DELAY_NOT_OVER);
					for(int i = 0; i < Math.min(collectTimes.size(), 5); i++){
						Util.DATE.setTime(collectTimes.get(i) - Calculator.currentTime);
						message.append(Util.NL).append(Util.DATE_FORMAT.format(Util.DATE));
					}
					JOptionPane.showMessageDialog(collectButton, message.toString(), L.TITLE+L.SEP+Calculator.gameName, JOptionPane.WARNING_MESSAGE);
				}
				else if(JOptionPane.showConfirmDialog(collectButton, L.ITEM_ADD_COLLECT.replace(Util.REP, name+L.ITEM_X+amount), L.TITLE, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
					for(int i = 0; i < amount; i++){
						removeDelayedCraft(false);
					}
					Calculator.calculate();
					updateCollect();
				}
			}
		});
		collectButton.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				if(SwingUtilities.isRightMouseButton(e)){
					if(JOptionPane.showConfirmDialog(collectButton, L.ITEM_FORCE_COLLECT.replace(Util.REP, name), L.TITLE+L.SEP+Calculator.gameName, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
						removeDelayedCraft(true);
						Calculator.calculate();
						updateCollect();
					}
				}
			}
		});

		craftField.setBackground(Util.COLOR_CRAFT);
		craftField.setHorizontalAlignment(SwingConstants.CENTER);
		craftField.setPreferredSize(Util.DIMENSION_COMPONENTS);
		craftField.addActionListener(e->craftItem(false));

		inventoryButton.setBackground(Util.COLOR_BUTTON);
		inventoryButton.setMargin(Util.INSETS_0);
		inventoryButton.setPreferredSize(Util.DIMENSION_COMPONENTS);
		inventoryButton.addActionListener(e->craftItem(false));
		inventoryButton.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				if(SwingUtilities.isRightMouseButton(e)){
					craftItem(true);
				}
			}
		});
		changeInventoryAmount(inventoryAmount);

		neededLabel.setOpaque(true);
		neededLabel.setPreferredSize(Util.DIMENSION_COMPONENTS);

		processLabel.setOpaque(true);
		processLabel.setPreferredSize(Util.DIMENSION_COMPONENTS);
	}


	/**
	 Checks if the item has the correct amount of recipe components to craft or enough inventory to disassemble.
	 @param amount The amount of the item to craft or disassemble.
	 @return Object[]{<br>boolean True if amount can be crafted or disassembled,<br>String Message describing missing components, null if no items are missing.<br>}
	 */
	public Object[] canCraft(int amount){
		boolean pass = true;
		StringBuilder message;
		if(amount > 0){
			message = new StringBuilder(L.ITEM_MISSING_COMPONENTS);
			for(int i = 0; i < recipeComponents.length; i++){
				if(recipeComponents[i].inventoryAmount < recipeAmounts[i] * amount){
					pass = false;
					message.append(Util.NL).append(recipeComponents[i].name).append(L.ITEM_X).append((recipeAmounts[i] * amount) - recipeComponents[i].inventoryAmount);
				}
			}
		}
		else{
			message = new StringBuilder(L.ITEM_MISSING_ITEM);
			if(inventoryAmount < amountPerCraft * (-amount)){
				pass = false;
				message.append(Util.NL).append(name).append(L.ITEM_X).append((amountPerCraft * (-amount)) - inventoryAmount);
			}
		}
		if(pass && productionGroup > 0){
			int timesAmount = 0;
			for(Item item : Calculator.items){
				if(item.productionGroup == productionGroup){
					timesAmount += item.collectTimes.size();
				}
			}
			if(timesAmount + amount > Calculator.productionGroupSlots[productionGroup]){
				pass = false;
				message = new StringBuilder(L.ITEM_PRODUCTION_FULL);
			}
		}
		return new Object[]{pass, pass ? null : message.toString()};
	}

	/**
	 Gets the amount this item is directly requested.
	 @return The amount.
	 */
	public int getRequestAmount(){
		int amount = 0;
		for(JTextField field : requestFields){
			if(field.getText().length() > 0){
				try{
					amount += Integer.parseInt(field.getText());
				}catch(NumberFormatException ignored){}
			}
		}
		return amount;
	}

	/**
	 Checks if an item is in the recipe.
	 @param item Item to check for.
	 @return True if the item is in the recipe.
	 */
	public boolean hasComponent(Item item){
		for(Item component : recipeComponents){
			if(component.equals(item)){
				return true;
			}
		}
		return false;
	}


	/**
	 Adds a components to the recipe.
	 @param component The component to add.
	 @param amount The amount of the component in the recipe.
	 */
	public void addComponent(Item component, int amount){
		recipeComponents = Util.resizeArray(recipeComponents, 1);
		recipeComponents[recipeComponents.length - 1] = component;
		recipeAmounts = Util.resizeArray(recipeAmounts, 1);
		recipeAmounts[recipeAmounts.length - 1] = amount;
	}

	public void addDelayedCraft(){
		long maxTime = Calculator.currentTime;
		if(collectChain){
			if(productionGroup > 0){
				for(Item item : Calculator.items){
					if(item.collectChain && item.productionGroup == productionGroup && item.collectTimes.size() > 0){
						maxTime = Math.max(maxTime, item.collectTimes.get(item.collectTimes.size() - 1));
					}
				}
			}
			else if(collectTimes.size() > 0){
				maxTime = collectTimes.get(collectTimes.size() - 1);
			}
		}
		collectTimes.add(maxTime + collectDelay);
		collectButton.setText(Util.EMPTY+collectTimes.size());
		collectCheck = true;
	}

	/**
	 Adds the amount to the inventory and changes button text.
	 @param amount The amount to add.
	 */
	public void changeInventoryAmount(int amount){
		inventoryAmount += amount;
		inventoryButton.setText(Util.EMPTY+inventoryAmount);
	}

	/**
	 Attempts to craft or disassemble this item.
	 @param force True if the item should be forced into the inventory.
	 */
	public void craftItem(boolean force){
		int amount = 1;
		try{
			amount = Integer.parseInt(craftField.getText());
		}catch(NumberFormatException ignored){}
		if(amount == 0){
			craftField.setText(null);
		}
		else{
			boolean pass = force;
			Object[] data = null;
			if(!pass){
				data = canCraft(amount);
				pass = (boolean) data[0];
			}
			if(pass){
				if(JOptionPane.showConfirmDialog(inventoryButton, (force ? amount > 0 ? L.ITEM_ADD_FORCE : L.ITEM_REMOVE_FORCE : amount > 0 ? group == 0 ? L.ITEM_ADD_COLLECT : L.ITEM_ADD_CRAFT : L.ITEM_REMOVE_CRAFT).replace(Util.REP, name+L.ITEM_X+(amount > 0 ? amount : -amount)), L.TITLE, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
					if(!force){
						for(int i = 0; i < recipeComponents.length; i++){
							recipeComponents[i].changeInventoryAmount(-(recipeAmounts[i] * amount));
						}
					}
					if(collectDelay == 0 || force || amount < 0){
						changeInventoryAmount(Math.max(force ? amount : amountPerCraft * amount, -inventoryAmount));
					}
					else{
						for(int i = 0; i < amount; i++){
							addDelayedCraft();
						}
						updateCollect();
					}
					craftField.setText(null);
					Calculator.calculate();
				}
			}
			else{
				JOptionPane.showMessageDialog(inventoryButton, data[1], L.TITLE+L.SEP+name, JOptionPane.WARNING_MESSAGE);
			}
		}
	}

	/**
	 Removes a component from the recipe.
	 @param component The component to remove.
	 */
	public void removeComponent(Item component){
		for(int i = 0; i < recipeComponents.length; i++){
			if(recipeComponents[i].equals(component)){
				recipeComponents[i] = null;
				recipeAmounts[i] = null;
				break;
			}
		}
		recipeComponents = Util.resizeArray(recipeComponents, -1);
		recipeAmounts = Util.resizeArray(recipeAmounts, -1);
	}

	public void removeDelayedCraft(boolean force){
		if(collectChain && force && collectTimes.size() > 1){
			long diff = collectTimes.get(0) - Calculator.currentTime;
			if(diff > 0){
				if(productionGroup > 0){
					for(Item item : Calculator.items){
						if(item.collectChain && item.productionGroup == productionGroup){
							for(int i = item.equals(this) ? 1 : 0; i < item.collectTimes.size(); i++){
								item.collectTimes.set(i, item.collectTimes.get(i) - diff);
							}
						}
					}
				}
				else{
					for(int i = 1; i < collectTimes.size(); i++){
						collectTimes.set(i, collectTimes.get(i) - diff);
					}
				}
			}
		}
		collectTimes.remove(0);
		collectButton.setText(Util.EMPTY+collectTimes.size());
		changeInventoryAmount(amountPerCraft);
		collectCheck = true;
	}

	/**
	 Resets and validates all item components.
	 */
	public void resetItem(){
		inventoryButton.setBackground(Util.COLOR_BUTTON);
		neededLabel.setBackground(Color.WHITE);
		processLabel.setBackground(Color.WHITE);
		int amount;
		for(JTextField field : requestFields){
			field.setBackground(Util.COLOR_REQUEST);
			if(field.getText().length() > 0){
				try{
					amount = Integer.parseInt(field.getText());
					if(amount <= 0){
						field.setText(null);
						field.setBackground(Util.COLOR_REQUEST);
					}
					else if(amount <= inventoryAmount){
						field.setBackground(Color.GREEN);
					}
				}catch(NumberFormatException e){
					field.setBackground(Color.RED);
				}
			}
		}
	}

	public void updateCollect(){
		if(collectCheck){
			if(collectTimes.size() == 0){
				collectCheck = false;
				collectButton.setBackground(Color.BLACK);
				collectButton.setEnabled(false);
				collectButton.setText(null);
			}
			else{
				collectButton.setBackground(Color.ORANGE);
				collectButton.setEnabled(true);
				int amount = 0;
				for(Long time : collectTimes){
					if(time <= Calculator.currentTime){
						amount++;
					}
				}
				if(amount > 0){
					collectButton.setBackground(new Color((int) ((1 - (amount / (double) collectTimes.size())) * 255), 255, 0));
					if(amount == collectTimes.size()){
						collectCheck = false;
					}
				}
			}
		}
	}


	/**
	 Creates request input fields for the item.
	 @param requestAmounts Initial amounts to fill the fields from load.  Null if not from load.
	 */
	public void createRequestFields(int[] requestAmounts){
		JTextField[] newRequestFields = new JTextField[Calculator.requestColumns];
		for(int i = 0; i < Calculator.requestColumns; i++){
			JTextField field = new JTextField();
			if(requestAmounts == null){
				try{
					field.setText(i < requestFields.length ? Util.EMPTY+Integer.parseInt(requestFields[i].getText()) : null);
				}catch(NumberFormatException ignored){}
			}
			else{
				if(requestAmounts[i] > 0){
					field.setText(Util.EMPTY+requestAmounts[i]);
				}
			}
			field.setBackground(Util.COLOR_REQUEST);
			field.setHorizontalAlignment(SwingConstants.CENTER);
			field.setPreferredSize(Util.DIMENSION_COMPONENTS);
			field.addActionListener(ACTION_LISTENER_REQUEST);
			field.getDocument().addDocumentListener(new DocumentListener(){
				public void insertUpdate(DocumentEvent e){
					update();
				}
				public void removeUpdate(DocumentEvent e){
					update();
				}
				public void changedUpdate(DocumentEvent e){
					update();
				}
				private void update(){
					field.setBackground(Color.YELLOW);
					if(field.getText().length() > 0){
						try{
							Integer.parseInt(field.getText());
						}catch(NumberFormatException e){
							field.setBackground(Color.RED);
						}
					}
				}
			});
			newRequestFields[i] = field;
		}
		requestFields = newRequestFields;
	}

	/**
	 Sets up group based on recipe depth.
	 */
	public void setupGroup(){
		int depth = 0;
		for(Item component : recipeComponents){
			if(component.group == -1){
				component.setupGroup();
			}
			depth = Math.max(depth, component.group + 1);
		}
		group = depth;
	}


	public int compareTo(Object o){
		Item oItem = (Item) o;
		if(group == oItem.group){
			if(productionGroup == oItem.productionGroup){
				if(collectDelay == oItem.collectDelay){
					return name.compareTo(oItem.name);
				}
				return collectDelay > oItem.collectDelay ? 1 : -1;
			}
			return productionGroup > oItem.productionGroup ? 1 : -1;
		}
		return group > oItem.group ? 1 : -1;
	}
}