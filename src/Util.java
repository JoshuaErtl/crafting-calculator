import javax.swing.*;
import java.awt.*;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.Function;

public class Util{
	public static final Color COLOR_BUTTON = new Color(180, 220, 255);
	public static final Color COLOR_CRAFT = new Color(220, 180, 180);
	public static final Color COLOR_REQUEST = new Color(255, 200, 150);

	public static final Date DATE = new Date();
	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm:ss.SSS");

	public static final Dimension DIMENSION_COMPONENTS = new Dimension(50, 20);

	public static final Insets INSETS_0 = new Insets(0, 0, 0, 0);

	public static final String EMPTY = "";
	public static final String NL = "\n";
	public static final String REP = "%S";

	/**
	 Displays a prompt for inputting a line of text.
	 @param titleAddition Additional text to display on titles.
	 @param initialText Initial text to display on the prompt.  Is checked against invalidation function before being displayed.
	 @param component Component to center windows around.
	 @param promptMessage Message that displays on the input prompt.
	 @param invalidationFunction Function to check if the input is invalid.  Must return true if it is invalid.
	 @param invalidMessage Message to display if the input is invalid.
	 @return The input.
	 */
	public static String displayInputPrompt(String titleAddition, String initialText, Component component, String promptMessage, Function<String, Boolean> invalidationFunction, String invalidMessage){
		String title = L.TITLE+L.SEP+titleAddition;
		String text = initialText == null || invalidationFunction.apply(initialText) ? null : initialText;
		do{
			text = (String) JOptionPane.showInputDialog(component, promptMessage, title, JOptionPane.QUESTION_MESSAGE, null, null, text);
			if(text != null && text.length() == 0){
				text = null;
			}
			if(text != null){
				if(invalidationFunction.apply(text)){
					JOptionPane.showMessageDialog(component, invalidMessage, title, JOptionPane.WARNING_MESSAGE);
				}
				else{
					break;
				}
			}
		}while(text != null);
		return text;
	}

	/**
	 Displays a prompt for choosing an item from a list.
	 @param titleAddition Additional text to display on titles.
	 @param items The backend items you want to select from.
	 @param component Component to center windows around.
	 @param emptyItemMessage Message to display if there are no items.
	 @param promptMessage Message that displays on the selection prompt.
	 @param itemNames The frontend items you want the selection to list.
	 @return The backend item selected.  Null if no choice was made.
	 */
	public static <T> T displaySelectionPrompt(String titleAddition, T[] items, Component component, String emptyItemMessage, String promptMessage, Object[] itemNames){
		String title = L.TITLE+L.SEP+titleAddition;
		if(items.length == 0){
			JOptionPane.showMessageDialog(component, emptyItemMessage, title, JOptionPane.WARNING_MESSAGE);
		}
		else{
			Object choice = JOptionPane.showInputDialog(component, promptMessage, title, JOptionPane.QUESTION_MESSAGE, null, itemNames, null);
			if(choice != null){
				for(int i = 0; i < items.length; i++){
					if(itemNames[i].equals(choice)){
						return items[i];
					}
				}
			}
		}
		return null;
	}

	/**
	 Displays a prompt for choosing an integer value.
	 @param titleAddition Additional text to display on titles.
	 @param promptMessage Message that displays on the selection prompt.
	 @param minValue Minimum value the spinner can reach.
	 @param initialValue Value the spinner initially has.
	 @param maxValue Maximum value the spinner can reach.
	 @param component Component to center windows around.
	 @return The value of the spinner.  Null if no choice was made.
	 */
	public static Integer displaySpinnerPrompt(String titleAddition, String promptMessage, int minValue, int initialValue, int maxValue, Component component){
		JPanel panel = new JPanel(new BorderLayout(0, 5));
		panel.add(new JLabel(promptMessage, SwingConstants.LEFT), BorderLayout.CENTER);
		JSpinner spinner = new JSpinner(new SpinnerNumberModel(initialValue, minValue, maxValue, 1));
		panel.add(spinner, BorderLayout.SOUTH);
		if(JOptionPane.showConfirmDialog(component, panel, L.TITLE+L.SEP+titleAddition, JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.OK_OPTION){
			try{
				spinner.commitEdit();
			}catch(ParseException ignored){}
			return (Integer) spinner.getValue();
		}
		return null;
	}

	/**
	 Resize and clone an array.  If not enough items in the original array are null, the furthest elements will be lost.
	 @param array The array to modify.
	 @param amount The amount to increase or decrease the array by.
	 @return The new array.
	 */
	public static <T> T[] resizeArray(T[] array, int amount){
		T[] newArray = (T[]) Array.newInstance(array.getClass().getComponentType(), Math.max(array.length + amount, 0));
		int insertIndex = 0;
		for(int i = 0; i < array.length && i < newArray.length; i++){
			if(array[i] != null){
				newArray[insertIndex] = array[i];
				insertIndex++;
			}
		}
		return newArray;
	}

	/**
	 Checks a string if it is considerably usable.
	 @param text The text to check.
	 @return True if invalid.
	 */
	public static boolean validString(String text){
		return text.isEmpty() || text.charAt(0) < 33 || text.charAt(0) > 126 || text.charAt(text.length() - 1) < 33 || text.charAt(text.length() - 1) > 126;
	}
}