The Crafting Calculator takes the modern idea of recipe based component creation in video games, as
well as the idea of production lines in any sense, and reverses the process to provide the user with
an easy to see representation of parts acquired, required, and completing.

Users are only asked to provide the fundamentals of an item for the Crafting Calculator to derive
the rest, of which this information is only as specific as the item itself, meaning most of the
information available can be seen as optional.
Item Parameters:
	- a unique name
	- the item's recipe, including quantity of other items
	- the amount produced per production
	- the assignment to a production group

These allow the Crafting Calculator to form a hierarchy of components, seperating base components
from increasingly more complex items.  Options such as production groups also allow for limitations
from hard resources, for instance if multiple items are produced using the same queue and cannot be
added if all slots are used.  These production rules in addition have a timeable aspect, allowing
for actual reminder of processing time.

Use of the Crafting Calculator is relatively straightforward, as once the data is provided,
functionality does not move from the single screen.  If there is a time when more data needs to be
added or changed, information will be dynamically resorted and adjusted to match the new rules.
The features of the main screen are as follows:
	- "Item" button : Only button available for new files, brings up the settings.
		Serves as header for item name column and always available options menu.
	- "Request" columns : Each vertical strip is considered a full request, and can only be entirely
		satisfied if the entire strip's amounts are met.  Each box only allows for number values to
		be inserted, invalid numbers will be removed and non-number data will be highlighted in red.  Yellow signifies a change from the previous confirmation, and confirmation is done through pressing "Enter" to accept all changes.  Green shows that an amount is met.  The "X" below each strip is assigned to the whole strip, therefore if it is green the whole request is fulfilled and can be cleared by pressing the assigned "X".  This will then remove the items used in this request.
	- "Craft" column : Overrides the default singular item function of each "Inv." button.
		Once a corresponding "Inv." button is used, the assigned "Craft" entry will be cleared.
		Pressing "Enter" will act on the positive value crafting or negative value disassembling
		action on the "Inv." button.  Disassembly is only usable if a negative value is placed in
		these fields.
	- "Inv." buttons : Shows the amount of each item stored in the "Inventory".  Left clicking will
		attempt to craft one of the item or the amount in "Craft", or disassemble by the amount in
		"Craft" if the number is negative.  Right clicking will force the addition of one of the
		item or the amount in "Craft", or force the removal by the amount in "Craft".  If an item
		cannot be crafted for any reason, a message will describe the cause.  If it is colored green,
		that item is specifically used in a request or recipe and can be crafted or added now.
		The buttons do not need to be green to use these buttons, it is to notify that the production
		queue has space available, the required components are available, etc., and will be needed
		now for a request itself or as a component down the line.
	- "Process" columns : Field position itself represents the chain of components down the recipe's
		specified.  This more forwardly illustrates the complexity of items more than the vertical
		sorting and spacing, the left signifies the basic, always gatherable items while the
		rightmost associated fields represent the complex items that can only be made through
		recipes.  The values signify how many times a crafting session needs to occur, as in
		clicking the "Inv." button this number of times to gather or craft enough of the item.
		For instance, if 2 of this item are requested and produces 2 when crafted, the "Process"
		value will read 1 due to only needing to be crafted 1 time yet produces 2.  If colored green,
		the item associated is a component in a further recipe and will be used if that recipe is
		crafted, therefore the item is both available but could decrease if a later item is made.
	- "Needed" column : The exact total more required of each item across the sum of request fields
		to the end production of all recipes.  If this value is greater than 0, more production or
		collection of that item is required by other requests.
	- "Collect" buttons : If an item has a production time to craft, collection of these items will
		be handled through these buttons.  Either in batch or sequential form, left clicking the
		button will add however many produced within the time specified to the inventory, with the
		amount shifting the color from orange to green.  If no items are produced, left clicking
		will display the times until the next few queued will take.  Right clicking will force the
		first item queued to be instantly produced, as well as remove its time from other items in
		the queue when produced individually.


Demo:  Two examples are given for demonstration purposes only, a "testgame" and "simcity" file:
testgame  = Simply illustrates the fundamental aspects
simcity   = Actual example using the item farming style gameplay of SimCity BuildIt to create the recipes